﻿using Dapper;
using Sloth.Repository.Configuration;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace Sloth.Repository.Helpers
{
    public class SqlDbContext
    {
        private readonly string _connectionString;

        public SqlDbContext(SqlConfiguration options)
        {
            _connectionString = options.ConnectionString;
        }

        private DbConnection GetConnection()
        {
            var connection = new SqlConnection(_connectionString);
            return connection;
        }

        public async Task<IEnumerable<T>> QueryAsync<T>(string sql, object param = null)
        {
            using (var connection = GetConnection())
            {
                await connection.OpenAsync().ConfigureAwait(false);
                return await connection.QueryAsync<T>(sql, param).ConfigureAwait(false);
            }
        }

        public async Task ExecuteAsync(string sql, object param = null)
        {
            using (var connection = GetConnection())
            {
                await connection.OpenAsync().ConfigureAwait(false);
                await connection.ExecuteAsync(sql, param).ConfigureAwait(false);
            }
        }

        public async Task<IEnumerable<T>> QueryAsyncSproc<T>(string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var connection = GetConnection())
            {
                await connection.OpenAsync().ConfigureAwait(false);
                return await connection.QueryAsync<T>(sql, param, transaction, commandTimeout, commandType);
            }
        }

        public async Task ExecuteAsyncSproc(string sql, object param = null, IDbTransaction transaction = null, bool buffered = true, int? commandTimeout = null, CommandType? commandType = null)
        {
            using (var connection = GetConnection())
            {
                await connection.OpenAsync().ConfigureAwait(false);
                await connection.ExecuteAsync(sql, param, transaction, commandTimeout, commandType);
            }
        }

        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TReturn>(string sql, Func<TFirst, TSecond, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null, string connectionStringName = "App", [CallerFilePath] string callerFilePath = null, [CallerMemberName] string callerMemberName = null, bool forceAudit = false)
        {
            return QueryAsync<TFirst, TSecond, DontMap, DontMap, DontMap, DontMap, DontMap, TReturn>(callerFilePath, callerMemberName, sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType, connectionStringName, forceAudit);
        }

        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TReturn>(string sql, Func<TFirst, TSecond, TThird, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null, string connectionStringName = "App", [CallerFilePath] string callerFilePath = null, [CallerMemberName] string callerMemberName = null, bool forceAudit = false)
        {
            return QueryAsync<TFirst, TSecond, TThird, DontMap, DontMap, DontMap, DontMap, TReturn>(callerFilePath, callerMemberName, sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType, connectionStringName, forceAudit);
        }

        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null, string connectionStringName = "App", [CallerFilePath] string callerFilePath = null, [CallerMemberName] string callerMemberName = null, bool forceAudit = false)
        {
            return QueryAsync<TFirst, TSecond, TThird, TFourth, DontMap, DontMap, DontMap, TReturn>(callerFilePath, callerMemberName, sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType, connectionStringName, forceAudit);
        }

        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null, string connectionStringName = "App", [CallerFilePath] string callerFilePath = null, [CallerMemberName] string callerMemberName = null, bool forceAudit = false)
        {
            return QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, DontMap, DontMap, TReturn>(callerFilePath, callerMemberName, sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType, connectionStringName, forceAudit);
        }

        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null, string connectionStringName = "App", [CallerFilePath] string callerFilePath = null, [CallerMemberName] string callerMemberName = null, bool forceAudit = false)
        {
            return QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, DontMap, TReturn>(callerFilePath, callerMemberName, sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType, connectionStringName, forceAudit);
        }

        public Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeven, TReturn>(string sql, Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeven, TReturn> map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null, string connectionStringName = "App", [CallerFilePath] string callerFilePath = null, [CallerMemberName] string callerMemberName = null, bool forceAudit = false)
        {
            return QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeven, TReturn>(callerFilePath, callerMemberName, sql, map, param, transaction, buffered, splitOn, commandTimeout, commandType, connectionStringName, forceAudit);
        }

        public async Task<IEnumerable<TReturn>> QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeven, TReturn>(string callerFilePath, string callerMemberName, string sql, Delegate map, dynamic param = null, IDbTransaction transaction = null, bool buffered = true, string splitOn = "Id", int? commandTimeout = null, CommandType? commandType = null, string connectionStringName = "App", bool forceAudit = false)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();

            try
            {
                using (var connection = GetConnection())
                {
                    var mappingType = new Type[] { typeof(TFirst), typeof(TSecond), typeof(TThird), typeof(TFourth), typeof(TFifth), typeof(TSixth), typeof(TSeven) };
                    var realMappingTypes = mappingType.Count(t => t != typeof(DontMap));

                    IEnumerable<TReturn> items = null;

                    await connection.OpenAsync();
                    var localeCreatedTransaction = false;
                    if (transaction == null)
                    {
                        transaction = connection.BeginTransaction(IsolationLevel.ReadUncommitted);
                        localeCreatedTransaction = true;
                    }

                    switch (realMappingTypes)
                    {
                        case 1:
                            items = await SqlMapper.QueryAsync<TReturn>(transaction.Connection, sql, param, transaction, commandTimeout, commandType);
                            break;
                        case 2:
                            items = await SqlMapper.QueryAsync<TFirst, TSecond, TReturn>(transaction.Connection, sql, (Func<TFirst, TSecond, TReturn>)map, param, transaction,buffered, splitOn, commandTimeout, commandType);
                            break;
                        case 3:
                            items = await SqlMapper.QueryAsync<TFirst, TSecond, TThird, TReturn>(transaction.Connection, sql, (Func<TFirst, TSecond, TThird, TReturn>)map, param, transaction, buffered, splitOn, commandTimeout, commandType);
                            break;
                        case 4:
                            items = await SqlMapper.QueryAsync<TFirst, TSecond, TThird, TFourth, TReturn>(transaction.Connection, sql, (Func<TFirst, TSecond, TThird, TFourth, TReturn>)map, param, transaction, buffered, splitOn, commandTimeout, commandType);
                            break;
                        case 5:
                            items = await SqlMapper.QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>(transaction.Connection, sql, (Func<TFirst, TSecond, TThird, TFourth, TFifth, TReturn>)map, param, transaction, buffered, splitOn, commandTimeout, commandType);
                            break;
                        case 6:
                            items = await SqlMapper.QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>(transaction.Connection, sql, (Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TReturn>)map, param, transaction, buffered, splitOn, commandTimeout, commandType);
                            break;
                        case 7:
                            items = await SqlMapper.QueryAsync<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeven, TReturn>(transaction.Connection, sql, (Func<TFirst, TSecond, TThird, TFourth, TFifth, TSixth, TSeven, TReturn>)map, param, transaction, buffered, splitOn, commandTimeout, commandType);
                            break;
                    }

                    if (localeCreatedTransaction)
                    {
                        transaction.Commit();
                    }

                    return items;
                }
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                stopwatch.Stop();
                //InsertLog(callerFilePath, callerMemberName, sql, param, success, stopwatch.ElaspedMiliseconds, forceAudit);
            }
        }
        private class DontMap { }
    }
}
