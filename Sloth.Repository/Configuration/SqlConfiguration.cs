﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Repository.Configuration
{
    public class SqlConfiguration
    {
        public string ConnectionString { get; set; }
    }
}
