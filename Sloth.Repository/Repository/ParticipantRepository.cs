﻿using Sloth.Model.Model.Participant;
using Sloth.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sloth.Repository.Repository
{
    public class ParticipantRepository : BaseRepository
    {
        public ParticipantRepository(SqlDbContext sqlDbContext) : base(sqlDbContext)
        {
        }

        public async Task<Participants> GetParticipants(GetParticipants getParticipants)
        {
            var participants = (await _sqlDbContext.QueryAsync<Participant>("Select Uid, FirstName, LastName, Age, CreatedDateUtc From Participant")).ToList();

            return new Participants
            {
                ParticipantList = participants
            };
        }

        public async Task<Participant> GetParticipant(GetParticipant getParticipant)
        {
            return (await _sqlDbContext.QueryAsync<Participant>("Select * from Participant Where Uid = @Uid", new { Uid = getParticipant.Uid })).FirstOrDefault();
        }

        public async Task<Participant> CreateParticipant(UpsertParticipant upsertParticipant)
        {
            return (await _sqlDbContext.QueryAsync<Participant>(@"
Declare @Id int
Insert into Participant([FirstName], [LastName], [Age])
Values(@FirstName, @LastName, @Age);

Set @Id = SCOPE_IDENTITY()

Select * from Participant Where Id = @Id
", upsertParticipant)).FirstOrDefault();
        }

        public async Task UpdateParticipant(UpsertParticipant upsertParticipant)
        {
            await _sqlDbContext.ExecuteAsync(@"Update Participant
                 Set FirstName = @FirstName, LastName = @LastName, Age = @Age
                Where Uid = @Uid", upsertParticipant);
        }

        public async Task DeleteParticipant(Guid uid)
        {
            await _sqlDbContext.ExecuteAsync(@"Delete from Participant Where Uid = @Uid", new { Uid = uid });
        }

        public async Task<Participants> Test()
        {
            var items = await _sqlDbContext.QueryAsync<Participants, UpsertParticipant, Participants>(@"", (participants, upsertParticipant) =>
            {
                return participants;
            }, 20, splitOn: "Id");

            return new Participants()
            {
            };
        }
    }
}
