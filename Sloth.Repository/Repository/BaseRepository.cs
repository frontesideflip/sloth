﻿using Dapper;
using Sloth.Repository.Helpers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;

namespace Sloth.Repository.Repository
{
    public abstract class BaseRepository
    {
        protected readonly SqlDbContext _sqlDbContext;

        protected BaseRepository(SqlDbContext sqlDbContext)
        {
            _sqlDbContext = sqlDbContext;
        }
    }
}
