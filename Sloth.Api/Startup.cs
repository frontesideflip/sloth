﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Sloth.Domain.Domain;
using Sloth.Repository.Repository;
using AutoMapper;
using Sloth.Repository.Configuration;
using Sloth.Repository.Helpers;

namespace Sloth.Api
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            if (env.IsDevelopment())
            {
                builder.AddUserSecrets<Startup>();
            }

            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // Add framework services.
            services.AddMvc();

            services.AddAutoMapper();

            services.AddSingleton(new SqlConfiguration()
            {
                ConnectionString = Configuration.GetSection("SqlConfiguration")["ConnectionString"]
            });

            services.AddTransient<SqlDbContext>();

            void AddTransients(IEnumerable<Type> types)
            {
                foreach (var type in types)
                {
                    services.AddTransient(type);
                }
            }

            //var baseRepository = typeof(BaseRepository);
            //var repositoriesToAdd = baseRepository.Assembly().GetTypes().Where(t => t.IsSubclassOf(baseRepository) && !t.IsAbstract);
            //AddTransients(repositoriesToAdd);

            //var baseService = typeof(BaseDomain);
            //var servicesToAdd = baseService.Assembly().GetTypes().Where(t => t.IsSubclassOf(baseRepository) && !t.IsAbstract);
            //AddTransients(servicesToAdd);

            services.AddTransient<ParticipantRepository>();
            services.AddTransient<ParticipantDomain>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            app.UseMvc();
        }
    }
}
