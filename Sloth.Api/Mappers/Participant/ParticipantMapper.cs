﻿using AutoMapper;
using Sloth.Dto.Dto.Participant;
using Sloth.Model.Model.Participant;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sloth.Api.Mappers
{
    public static class ParticipantMapper
    {
        public class ParticipantMapperProfile : Profile
        {
            public ParticipantMapperProfile()
            {
                CreateMap<GetParticipantsDto, GetParticipants>();
                CreateMap<GetParticipantDto, GetParticipant>();
                CreateMap<Participants, ParticipantsDto>();
                CreateMap<Participant, ParticipantDto>();
                CreateMap<ParticipantDto, Participant>();
                CreateMap<UpsertParticipantDto, UpsertParticipant>();
                CreateMap<UpsertParticipant, UpsertParticipantDto>();
            }
        }

        public static GetParticipants ToModel(this GetParticipantsDto dto) => Mapper.Map<GetParticipants>(dto);

        public static ParticipantsDto ToDto(this Participants model) => Mapper.Map<ParticipantsDto>(model);

        public static GetParticipant ToModel(this GetParticipantDto dto) => Mapper.Map<GetParticipant>(dto);

        public static ParticipantDto ToDto(this Participant model) => Mapper.Map<ParticipantDto>(model);

        public static UpsertParticipant ToModel(this UpsertParticipantDto dto) => Mapper.Map<UpsertParticipant>(dto);
    }
}
