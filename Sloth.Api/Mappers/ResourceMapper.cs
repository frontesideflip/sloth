﻿using AutoMapper;
using Sloth.Dto.Dto;
using Sloth.Model.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Sloth.Api.Mappers
{
    public class ResourceMapper : Profile
    {
        public ResourceMapper()
        {
            CreateMap<Resource, ResourceDto>();
            CreateMap<GetResource, GetResourceDto>();
            CreateMap<GetResources, GetResourcesDto>();
            CreateMap<ResourceDto, Resource>();
            CreateMap<GetResourceDto, GetResource>();
            CreateMap<GetResourcesDto, GetResources>();
        }
    }
}
