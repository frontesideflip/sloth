﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Sloth.Domain.Domain;
using Sloth.Dto.Dto.Participant;
using Sloth.Api.Mappers;


namespace Sloth.Api.Controllers
{
    [Route("api/participants")]
    public class ParticipantController : BaseController
    {
        public ParticipantDomain _participantDomain;

        public ParticipantController(ParticipantDomain participantDomain)
        {
            _participantDomain = participantDomain;
        }

        [HttpGet]
        public async Task<IActionResult> Get(GetParticipantsDto dto)
        {
            var model = dto.ToModel();

            var results = await _participantDomain.GetParticipants(model);

            return Ok(results.ToDto());
        }

        [HttpGet("{uid:guid}")]
        public async Task<IActionResult> Get([FromRoute] Guid uid)
        {
            var model = new GetParticipantDto() { Uid = uid }.ToModel();

            var result = await _participantDomain.GetParticipant(model);

            return Ok(result.ToDto());
        }

        [HttpPost]
        public async Task<IActionResult> Post([FromBody] UpsertParticipantDto dto)
        {
            var model = dto.ToModel();

            var result = await _participantDomain.CreateParticipant(model);

            return Ok(result.ToDto());
        }

        [HttpPut("{uid:guid}")]
        public async Task<IActionResult> Put([FromRoute] Guid uid, [FromBody]UpsertParticipantDto dto)
        {
            var model = dto.ToModel();

            await _participantDomain.UpdateParticipant(model, uid);

            return Ok();
        }

        [HttpDelete("{uid:guid}")]
        public async Task<IActionResult> Delete(Guid uid) 
        {
            await _participantDomain.DeleteParticipant(uid);

            return Ok();
        }
    }
}
