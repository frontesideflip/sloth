﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Dto.Dto
{
    public class ResourceDto
    {
        public Guid Uid { get; set; }
        public DateTime CreatedDateUtc { get; set; }
        public bool Enabled { get; set; }
    }
}
