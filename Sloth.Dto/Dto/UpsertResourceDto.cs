﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Dto.Dto
{
    public class UpsertResourceDto
    {
        public bool Enabled { get; set; } = true;
    }
}
