﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Dto.Dto
{
    public class GetResourcesDto
    {
        public IEnumerable<Guid> Uids { get; set; }
        public bool Enabled { get; set; } = true;
    }
}
