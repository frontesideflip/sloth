﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Dto.Dto.Participant
{
    public class ParticipantsDto
    {
        public List<ParticipantDto> ParticipantList { get; set; }
    }
}
