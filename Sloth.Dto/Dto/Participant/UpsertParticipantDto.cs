﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Dto.Dto.Participant
{
    public class UpsertParticipantDto : UpsertResourceDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
