﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Dto.Dto
{
    public class GetResourceDto
    {
        public Guid Uid { get; set; }
        public bool Enabled { get; set; } = true;
    }
}
