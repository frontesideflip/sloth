﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Model.Model.Participant
{
    public class UpsertParticipant : UpsertResource
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
