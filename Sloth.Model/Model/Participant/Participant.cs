﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Model.Model.Participant
{
    public class Participant : Resource
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
    }
}
