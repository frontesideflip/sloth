﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Model.Model
{
    public class Resource
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public DateTime CreatedDateUtc { get; set; }
        public bool Enabled { get; set; }
    }
}
