﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Model.Model
{
    public class UpsertResource
    {
        public Guid Uid { get; set; }
        public bool Enabled { get; set; } = true;
    }
}
