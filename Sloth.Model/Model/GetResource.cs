﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Model.Model
{
    public class GetResource
    {
        public int Id { get; set; }
        public Guid Uid { get; set; }
        public bool Enabled { get; set; } = true;
    }
}
