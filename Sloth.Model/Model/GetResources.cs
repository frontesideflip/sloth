﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Sloth.Model.Model
{
    public class GetResources
    {
        public IEnumerable<int> Ids { get; set; }
        public IEnumerable<Guid> Uids { get; set; }
        public bool Enabled { get; set; } = true;
    }
}
