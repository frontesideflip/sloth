﻿using Sloth.Model.Model.Participant;
using Sloth.Repository.Repository;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Sloth.Domain.Domain
{
    public class ParticipantDomain : BaseDomain
    {
        public ParticipantRepository _participantRepository;

        public ParticipantDomain(ParticipantRepository participantRepository)
        {
            _participantRepository = participantRepository;
        }

        public async Task<Participants> GetParticipants(GetParticipants getParticipants)
        {
            return await _participantRepository.GetParticipants(getParticipants);
        }

        public async Task<Participant> GetParticipant(GetParticipant getParticipant)
        {
            return await _participantRepository.GetParticipant(getParticipant);
        }

        public async Task<Participant> CreateParticipant(UpsertParticipant upsertParticipant)
        {
            return await _participantRepository.CreateParticipant(upsertParticipant);
        }

        public async Task UpdateParticipant(UpsertParticipant upsertParticipant, Guid uid)
        {
            var participant = await GetParticipant(new GetParticipant() { Uid = uid });

            if (participant != null)
            {
                upsertParticipant.Uid = uid;
                await _participantRepository.UpdateParticipant(upsertParticipant);
            }
        }

        public async Task DeleteParticipant(Guid uid)
        {
            var participant = await GetParticipant(new GetParticipant() { Uid = uid });

            if (participant != null)
            {
                await _participantRepository.DeleteParticipant(uid);
            }
        }
    }
}
